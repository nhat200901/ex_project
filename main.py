import student

class main:
    def main(self):
        s = student.Student()

        while True:
            print("")
            print("Chọn chức năng muốn thực hiện:")
            print("Nhập 1: Thêm sinh viên")
            print("Nhập 2: Xem danh sách sinh viên")
            print("Nhập 0: Thoát")
            x = int(input())

            if x == 0:
                break
            elif x == 1:
                s.addStudent()
            elif x == 2:
                s.showStudent()

m = main()
m.main()
