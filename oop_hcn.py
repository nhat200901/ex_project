class HinhChuNhat:
    def __init__(self, dai, rong):
        self.dai = dai
        self.rong = rong

    def get_chuvi(self):
        return (self.dai + self.rong)*2

    def get_dientich(self):
        return self.dai * self.rong

my_hcn = HinhChuNhat(2,3)

chuvi = my_hcn.get_chuvi()
dientich = my_hcn.get_dientich()

print(f"chu vi la: {chuvi}")
print(f"dien tich la: {dientich}")